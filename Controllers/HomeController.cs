﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Diagnostics;
using WebForm.Models;
using Microsoft.Extensions.Configuration;

namespace WebForm.Controllers
{
    public class HomeController : Controller
    {
        private readonly IConfiguration configuration;
        private static List<ResponseModel> responseModels = new List<ResponseModel>();


        [HttpGet]
        public IActionResult Index()
        {
            string sql = "select * from response";

            responseModels = DataLibrary.DataAccess.LoadedData<ResponseModel, dynamic>(sql, new { }, configuration.GetConnectionString("default"));
            return View(responseModels);
        }




        [HttpGet]
        public IActionResult FormInput() => View();

        [HttpPost]
        public IActionResult FormInput(ResponseModel response)
        {
            // Проверка на валидность
            if(ModelState.IsValid)
            {
                responseModels.Add(response); // Передаём весь response формы 
                return View("ThanksVeiw", response);
            } 
            else
            {
                return View();
            }
            // Метод принимающий первым параметром Имя View, объект
            
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error() { return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier }); }
    }
}
