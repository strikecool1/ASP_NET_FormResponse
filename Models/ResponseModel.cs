﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebForm.Models
{
    public class ResponseModel
    {
        [Required(ErrorMessage = "Имя не может быть пустым!")]
        public string Name { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public bool ?WillAttend { get; set; }
    }
}
